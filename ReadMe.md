## What is this a About.

The purpose of this Project is to develop a tool for Pentesting and to help to develop BLE devices. It focuses on Combining established BLE Tools (Ubertooth One & Wireshark, Gatttool|BetterCap, Bluez) to build a Device with all functionatialitees needed for BLE Pentesting and BLE Development. 

## RoadMap

	                                ┌──────────┐
	                                │   Start  │
	                                └─────┬────┘
	                                      │
	                                      ├─────────────────────────────────────────────────────────────┐
	                    ┌─────────────────┴───────────────┬───┐                                         │
	                    │ Gatt Scanning Module (BetterCap)│   │                                         │
	                    └─────────────────┬───────────────┴───┘                                         │
	                                      │                                                             │
	                ┌─────────────────────┴─────────────────────┐                                       │
	                │                                           │                                       │
	    ┌───────────┴───────────┬───┐       ┌───────────────────┴───────────────┬───┐       ┌───────────┴───────┬───┐
	    │ Ble Sniffing:         │   │       │ Gatt S/C Prototyping Framework    │   │       │ Web Interface     │   │
	    │ Bluefruit LE Sniffer  │   │       └───────────────────┬───────────────┴───┘       └───────────┬───────┴───┘
	    │ Ubertooth One         │   │       ┌───────────────────┴───────────────┬───┐                   │
	    └───────────┬───────────┴───┘       │ 8086 Pi Cluster HAT Configuration │   │                   │
	                │                       └───────────────────┬───────────────┴───┘                   │
	                │                       ┌───────────────────┴───────────────┬───┐                   │
	                │                       │ Man In the Middle Module          │   │                   │
	                │                       └───────────────────┬───────────────┴───┘                   │
	                │                                           │                                       │
	                └─────────────────────┬─────────────────────┘                                       │
	                                      │                                                             │
	                    ┌─────────────────┴─────────────┬───┐                                           │
	                    │ Wire shark Packet Capturing   │   │                                           │
	                    └─────────────────┬─────────────┴───┘                                           │
	                                      │                                                             │
	                    ┌─────────────────┴─────────────┬───┐                                           │
	                    │ SIEM Event Logging:           │   │                                           │
	                    │ Graylog                       │   │                                           │
	                    │ Kibana (Maybe)                │   │                                           │
	                    └─────────────────┬─────────────┴───┘                                           │
	                                      └─────────────────────────────────────────────────────────────┘



	┌─────────────────────────┐
	│ Special Chars:          │
	│ ┌ ─ ┐  ┤ │ ├ └ ┘  ┬ ┴ ┼ │
	└─────────────────────────┘

## Goals

```
┌───────────────────────────────────────────┐   ┌───────────────────────────────────────────┐
│             Theoretical Part              │   │              Practiacal Part              │
└─────────────────────┬─────────────────────┘   └─────────────────────┬─────────────────────┘
                      │                                               │
        ┌─────────────┴─────────────┐                   ┌─────────────┴─────────────┐
        │   Fundamental Research    │                   │ Implementations of Basics │
        ├───────────────────────────┤                   ├───────────────────────────┤
        │ BLE Stack                 │                   │ GATT Scanner              │
        │ GATT Structure            │                   │ Client Server Mock Up API │
        │ BLE Security Protocols    │                   │ Framework Basics          │
        └─────────────┬─────────────┘                   │ Hardware Compabilities    │
                      │                                 └─────────────┬─────────────┘
        ┌─────────────┴─────────────┐                                 │
        │  Vulnerabilitiy Analysis  │                                 │
        ├───────────────────────────┤                                 │
        │ Man In the Middle         │                                 │
        │ Eavesdropping             │                                 │
        │ Spoofing                  │                                 │
        └─────────────┬─────────────┘                                 │
                      │                                               │
        ┌─────────────┴─────────────┐                   ┌─────────────┴─────────────┐
        │      Threat Modeling      ├───── Findings ───>│     Pentesting Tools      │
        └───────────────────────────┘                   ├───────────────────────────┤
                                                        │ Vulnerablity Exploitation │
                                                        │ ...                       │
                                                        └───────────────────────────┘
```


